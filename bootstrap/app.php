<?php
use App\classes\AppContainer;

// session_start();

require __DIR__ . '/../vendor/autoload.php';
// spl_autoload_register(function ($classname) {
// require ("../classes/" . $classname . ".php");
// });

// Instantiate the app
define("CONFIG", require __DIR__ . '/../app/config/local.config.php');
// $app      = new App($settings);
$app = AppContainer::getInstance(CONFIG);

// Set up dependencies
require __DIR__ . '/../app/dependencies.php';

// Invoking DB
$app->getContainer()->get("db");

// Adding Middleware
// require __DIR__ . '/../app/middlewares.php';

// Automatically load routes files
$routers = glob(__DIR__ . '/../app/routers/*.routes.php');
foreach ($routers as $router) {
    require $router;
}
