<?php
namespace App\Controllers;

use App\Models\User;
use \App\Classes\DefaultMapper;
use \Interop\Container\ContainerInterface;

class UserController extends DefaultMapper
{

    protected $ci;
    protected $logger;

    //Constructor
    public function __construct(ContainerInterface $ci)
    {
        $this->ci         = $ci;
        $this->app        = \App\Classes\AppContainer::getInstance(CONFIG);
        $this->logger     = $this->ci->get('logger');
    }

    /*
     * Ex : /user
     */
    public function index($request, $response, $args)
    {
        // Accessing monolog intialized in App
        $this->logger->addInfo("UserController:index()");
        $this->logger->addInfo("UserController : Listing users");

        $users = User::all();
        // error_log(print_r($users, true));

        $code           = 200;
        $data["status"] = "success";
        $data["data"]   = array("users" => $users);

        return $response->withStatus($code)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, $this->ci->get('jsonOptions')));

    }

    /*
     * Ex : /user/{id}
     */
    public function show($request, $response, $args)
    {
        $id = $args['id'];

        // monolog
        $this->logger->addInfo("UserController:show($id)");

        $user = User::where('id', $id)->first();
        // error_log(print_r($user, true));

        if ($user == null) {
            $data["status"] = "fail";
            $data["data"]   = "user don't exists";
        } else {

            $data["status"] = "success";
            $data["data"]   = array("user" => $user);
        }

        return $response->withStatus(200)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, $this->ci->get('jsonOptions')));

        return $response->withJson($user);
    }

    /*
     * Ex : /user/
     */
    public function store($request, $response, $args)
    {
        // monolog
        $this->logger->addInfo("UserController:store()");

        $inputs = $request->getBody();
        $inputs = json_decode($inputs, true);
        $this->logger->addDebug("User Details:", $inputs);

        // $headers = $request->getHeaders();
        // foreach ($headers as $name => $values) {
        //     // echo $name . ": " . implode(", ", $values);
        //     $logger->addInfo($name . ": " . implode(", ", $values));
        // }

        $user                 = new User();
        $user->username       = $inputs['username'];
        $user->first_name     = $inputs['first_name'];
        $user->last_name      = $inputs['last_name'];
        $user->password       = $inputs['password'];
        $user->is_active      = $inputs['is_active'];
        $user->code           = $inputs['code'];
        $user->remember_token = $inputs['remember_token'];
        $user->login_count    = $inputs['login_count'];
        $status               = $user->save();

        if ($status) {
            $logger->addInfo("User created, ID:" . $user->id);
            $data["status"] = "success";
            $data["data"]   = array("id" => $user->id);
            $code           = 201;
        } else {
            $logger->addDebug("User not created");
            $data["status"] = "fail";
            $data["data"]   = "User not created";
            $code           = 200;
        }

        return $response->withStatus($code)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, $this->ci->get('jsonOptions')));

    }

    /*
     * Ex : /user/
     */
    public function update($request, $response, $args)
    {
        $id = $args['id'];

        $logger = $this->ci->get('logger');
        $logger->addInfo("UserController:update($id)");

        $inputs = $request->getBody();
        $inputs = json_decode($inputs, true);

        $user = User::find($id);

        // if ($user->isEmpty()) {
        //     $logger->addInfo("User not updated, don't exist, passed ID:" . $id);
        //     $data["status"]  = "fail";
        //     $data["data"] = "User don't exists";
        // } else {
        $user->username       = $inputs['username'];
        $user->first_name     = $inputs['first_name'];
        $user->last_name      = $inputs['last_name'];
        $user->password       = $inputs['password'];
        $user->is_active      = $inputs['is_active'];
        $user->code           = $inputs['code'];
        $user->remember_token = $inputs['remember_token'];
        $user->login_count    = $inputs['login_count'];
        $status               = $user->save();
        $this->logger->addInfo("User updated, ID:" . $id);

        if ($status) {
            $data["status"]  = "success";
            $data["message"] = "User updated";
        }

        return $response->withStatus(200)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, $this->ci->get('jsonOptions')));

    }

    /*
     * Ex : /user/
     */
    public function destroy($request, $response, $args)
    {
        $id = $args['id'];

        $this->logger->addInfo("UserController:destroy($id)");

        $status = User::destroy($id);

        if (!$status) {
            $this->logger->addInfo("User not deleted, don't exist, passed ID:" . $id);
            $data["status"] = "fail";
            $data["data"]   = "User don't exists";
        } else {
            $this->logger->addInfo("User deleted, ID was:" . $id);

            $data["status"] = "success";
            $data["data"]   = "User deleted";
        }

        return $response->withStatus(200)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, $this->ci->get('jsonOptions')));

    }

    /*
     * Ex : /user/moreaction
     */
    public function moreaction($request, $response, $args)
    {

        $this->logger->addInfo("UserController:moreaction()");

        $data["status"] = "success";
        $data["data"]   = "You can do moreaction";

        return $response->withStatus(200)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, $this->ci->get('jsonOptions')));

    }
}
