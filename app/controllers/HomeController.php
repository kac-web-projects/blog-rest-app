<?php
namespace App\Controllers;

use \Interop\Container\ContainerInterface;

class HomeController
{

    protected $ci;

    //Constructor
    public function __construct(ContainerInterface $ci)
    {
        $this->ci = $ci;
    }

    public function index($request, $response, $args)
    {
        // Accessing monolog intialized in App
        $logger = $this->ci->get('logger');

        $logger->addInfo("IndexController:index()");

        $data["status"] = "success";
        $data["data"]   = "Welcome to BlogApp Restful";

        return $response->withStatus(200)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, $this->ci->get('jsonOptions')));

    }

}
