<?php
namespace App\Controllers\Auth;

use \Interop\Container\ContainerInterface;

class AuthController
{

    protected $ci;

    //Constructor
    public function __construct(ContainerInterface $ci)
    {
        $this->ci = $ci;
    }

    public function doLogin($request, $response, $args)
    {
        // Accessing monolog intialized in App
        $logger = $this->ci->get('logger');

        $logger->addInfo("AuthController:doLogin()");
        // $response->getBody()->write("Trying to login");

        return $response;
    }

    // public function logout($request, $response, $args)
    // {
    //     // Accessing monolog intialized in App
    //     $logger = $this->ci->get('logger');

    //     $logger->addInfo("AuthController:doLogin()");
    //     // $response->getBody()->write("Trying to login");
        
    //     return $response;
    // }

}
