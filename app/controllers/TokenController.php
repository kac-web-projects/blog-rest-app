<?php
namespace App\Controllers;

use Firebase\JWT\JWT;
use Tuupola\Base62;
use \Interop\Container\ContainerInterface;

class TokenController
{
    protected $token;

    protected $ci;

    //Constructor
    public function __construct(ContainerInterface $ci)
    {
        $this->ci = $ci;
    }

    public function getToken($request, $response, $args)
    {
        // Accessing monolog intialized in App
        $logger = $this->ci->get('logger');

        $logger->addInfo("TokenController:getToken()");
        $logger->addDebug("args : " . print_r($args, true));

        $requested_scopes = $request->getParsedBody();

        // Define custom scope and check it later whever applicable
        $valid_scopes = [
            "component.creation",
            "component.reading",
            "component.updation",
            "component.deletion",
            "component.listing",
            "component.all",
        ];

        $scopes = array_filter($requested_scopes, function ($needle) use ($valid_scopes) {
            return in_array($needle, $valid_scopes);
        });

        $now    = new \DateTime();
        $future = new \DateTime("now +2 hours");
        $server = $request->getServerParams();

        $jti = Base62::encode(random_bytes(16));

        $payload = [
            "iat"   => $now->getTimeStamp(),
            "exp"   => $future->getTimeStamp(),
            "jti"   => $jti,
            "sub"   => $server["PHP_AUTH_USER"],
            "scope" => $scopes,
        ];

        $secret = "supersecretkeynevercommitthis";
        // $secret         = getenv("JWT_SECRET");
        $token          = JWT::encode($payload, $secret, "HS256");
        $data["status"] = "ok";
        $data["token"]  = $token;
        $this->token    = $token;

        return $response->withStatus(201)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }

    public function getDump($request, $response, $args)
    {
        // print_r($this->token);
        error_log(print_r($this->token, true));

        return $response->withStatus(200)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($this->token, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

    }

}
