<?php
// Dependency Injection Containers
// -----------------------------------------------------------------------------
$cont = $app->getContainer();

// Monolog for logs
// -----------------------------------------------------------------------------
$cont['logger'] = function ($c) {
    $logger       = new \Monolog\Logger('SlimBlogApp');
    $file_handler = new \Monolog\Handler\StreamHandler("../storage/logs/app.log");
    $logger->pushHandler($file_handler);

    return $logger;
};

// Error Handling
// -----------------------------------------------------------------------------
//Override the default Not Found Handler
$cont['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        $data["status"] = "fail";
        $data["data"]   = "Route/Page don't exists";
        return $c['response']
            ->withStatus(404)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, $c['jsonOptions']));
    };
};

// Read more : http://php.net/manual/en/json.constants.php
$cont['jsonOptions'] = JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT;

// Service factory for the ORM
$cont['db'] = function ($cont) {
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($cont['settings']['db']);

    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
};

// Home (1 way of doing it)
use App\Controllers\HomeController;

$cont[\HomeController::class] = function ($c) {
    return new HomeController($c);
};

use App\Controllers\UseActionController;

$cont[\UseActionController::class] = function ($c) {
    return new App\Controllers\UseActionController($c);
};

// WithCi
$cont[\WithCiController::class] = function ($c) {
    return new App\Controllers\WithCiController($c);
};

// Token
$cont['TokenController'] = function ($c) {
    return new App\Controllers\TokenController($c);
};

// User
$cont['UserController'] = function ($c) {
    return new App\Controllers\UserController($c);
};

// Radacct
$cont['RadacctController'] = function ($c) {
    return new App\Controllers\RadacctController($c);
};

// Radcheck
$cont['RadcheckController'] = function ($c) {
    return new App\Controllers\RadcheckController($c);
};
