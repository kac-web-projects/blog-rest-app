<?php

/* Basic auth for 1st call */
use \Slim\Middleware\HttpBasicAuthentication;
$app->add(new HttpBasicAuthentication([
    "path"        => "/",
    "passthrough" => ["/api"],
    "logger"      => $cont['logger'],
    "users"       => [
        "dashboard" => "d0shb0ard",
        "ci"        => "c1",
    ],
    "error"       => function ($request, $response, $arguments) {
        $data            = [];
        $data["status"]  = "error";
        $data["message"] = $arguments["message"];
        return $response->write(json_encode($data, JSON_UNESCAPED_SLASHES));
    },
]));

/* JWT auth for other calls */
$cont["jwt"] = function ($cont) {
    return new StdClass;
};

// Need token to access APIs
use \Slim\Middleware\JwtAuthentication;
$app->add(new \Slim\Middleware\JwtAuthentication([
    "path"        => "/api/",
    "passthrough" => ["/token"],
    "secure"      => false,
    // "relaxed"     => ["localhost", "staging.mywifi.com"],
    "logger"      => $cont['logger'],
    "secret"      => "supersecretkeynevercommitthis",
    "callback"    => function ($request, $response, $arguments) use ($cont) {
        $cont['logger']->addInfo("JWT succeeded");
        $cont["jwt"] = $arguments["decoded"];
        error_log(print_r($arguments["decoded"], true));
    },

    "error"       => function ($request, $response, $arguments) {
        $data            = [];
        $data["status"]  = "error";
        $data["message"] = $arguments["message"];
        return $response->write(json_encode($data, JSON_UNESCAPED_SLASHES));
    },

]));
