<?php
namespace App\Classes;

abstract class DefaultMapper
{
    // Force Extending class to define this method
    abstract protected function index($request, $response, $args);
    abstract protected function show($request, $response, $args);
    abstract protected function store($request, $response, $args);
    abstract protected function update($request, $response, $args);
    abstract protected function destroy($request, $response, $args);

    // abstract protected function create($request, $response, $args);
    // abstract protected function edit($request, $response, $args);

    protected $app;
    public function mapper()
    {
        // $this->app->map(['GET', 'POST'], '/method', [$this, 'method']);

        // List
        // $this->app->map(['GET'], '', [$this, 'index']);
        $this->app->map(['GET'], '/', [$this, 'index']);

        // Show
        $this->app->map(['GET'], '/{id:[0-9]+}', [$this, 'show']);
        $this->app->map(['GET'], '/{id:[0-9]+}/', [$this, 'show']);

        // Store
        $this->app->map(['POST'], '', [$this, 'store']);
        $this->app->map(['POST'], '/', [$this, 'store']);

        // Update
        $this->app->map(['PUT', 'PATCH'], '/{id}', [$this, 'update']);

        // Delete
        $this->app->map(['DELETE'], '/{id}', [$this, 'destroy']);

        // (Not required currently)
        // $this->app->map(['GET'], '/create', [$this, 'create']);
        // $this->app->map(['GET'], '/create/', [$this, 'create']);
        // $this->app->map(['GET'], '/{id}/edit', [$this, 'edit']);
    }

}
