<?php

// User Component
// -----------------------------------------------------------------------------

//using DefaultMapper mechanism
$app->group('/api/user', 'UserController:mapper');

//using custom/additional handler/mapper
$app->get('/api/user/moreaction', 'UserController:moreaction');

// $app->group('/api/user', function () {

// $this->get('create', 'UserController:create');
// $this->get('/{id}/edit', 'UserController:edit');
// $this->put('{id}', 'UserController:update');
// });
